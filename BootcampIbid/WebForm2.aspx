﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="BootcampIbid.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Calculadora</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblCalculadora" runat="server" Text="Calculadora"></asp:Label>
        </div>
        <p>
            <asp:Label ID="lblNumeroUm" runat="server" Font-Bold="True" Text="Pirmeiro Número"></asp:Label>
        </p>
        <p>
            <asp:TextBox ID="txtValorUm" runat="server"></asp:TextBox>
            <asp:DropDownList ID="ddlOperacao" runat="server" Font-Bold="True" Font-Strikeout="False">
                <asp:ListItem>X</asp:ListItem>
                <asp:ListItem>-</asp:ListItem>
                <asp:ListItem>+</asp:ListItem>
                <asp:ListItem>/</asp:ListItem>
                <asp:ListItem></asp:ListItem>
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="lblNumeroDois" runat="server" Text="Segundo Número" Font-Bold="True"></asp:Label>
        </p>
        <p>
            <asp:TextBox ID="txtValorDois" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Button ID="btnConfirmarOperacao" runat="server" OnClick="btnConfirmarOperacao_Click" Text="Calcular" BackColor="#009933" Font-Bold="True" ForeColor="Black" />
        </p>
        <p>
            <asp:Label ID="lblMenssagemErro" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            <asp:Label ID="lblMenssagem" runat="server" Font-Bold="True"></asp:Label>
        </p>
    </form>
</body>
</html>
