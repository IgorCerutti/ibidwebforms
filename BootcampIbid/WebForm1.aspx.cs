﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootcampIbid
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnConfirmarDia_Click(object sender, EventArgs e)
        {
            List<string> diasSelecionados = new List<string>();

            foreach(ListItem item in chkDiasdaSemana.Items)
            {
                if (item.Selected)
                {
                    diasSelecionados.Add(item.Value);
                }
            }
            lblResultadoGravadoDias.Text = $"Dias selecionados: {string.Join(", ", diasSelecionados)}";


        }

    }
}