﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Panel.aspx.cs" Inherits="BootcampIbid.Panel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="Panel1" runat="server" Height="536px" Width="515px">
                <asp:Panel ID="Panel2" runat="server" Height="173px" Width="227px" Visible="true">
                    <asp:Label ID="lblPanel" runat="server" Text="Informações pessoais"></asp:Label>
                    <br />
                    <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                    <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblSobrenome" runat="server" Text="Sobrenome:"></asp:Label>
                    <asp:TextBox ID="txtSobrenome" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblGenero" runat="server" Text="Gênero:"></asp:Label>
                    <asp:TextBox ID="txtGenero" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblCelular" runat="server" Text="Celular:"></asp:Label>
                    <asp:TextBox ID="txtCelular" runat="server"></asp:TextBox>
                    <br />
                    <br />
                    <asp:Button ID="btnProximoUm" runat="server" OnClick="btnProximoUm_Click" Text="Próximo" />
                    <asp:Label ID="lblMenssagemErro" runat="server" ForeColor="Red"></asp:Label>
                </asp:Panel>

                <asp:Panel ID="Panel3" runat="server" Height="166px" Width="231px" Visible="false">
                    <asp:Label ID="lblEnderecoPanel" runat="server" Text="Detalhes do endereço"></asp:Label>
                    <br />
                    <asp:Label ID="lblEndereco" runat="server" Text="Endereço:"></asp:Label>
                    <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblCidade" runat="server" Text="Cidade:"></asp:Label>
                    <asp:TextBox ID="txtCidade" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblCep" runat="server" Text="CEP:"></asp:Label>
                    <asp:TextBox ID="txtCep" runat="server"></asp:TextBox>
                    <br />
                    <asp:Button ID="btnVoltarUm" runat="server" OnClick="btnVoltarUm_Click" Text="Voltar" />
                    <asp:Button ID="btnProximoDois" runat="server" OnClick="btnProximoDois_Click" Text="Próximo" Width="51px" />
                    <asp:Label ID="lblMenssagemErro2" runat="server" ForeColor="Red"></asp:Label>
                </asp:Panel>

                <asp:Panel ID="Panel4" runat="server" Height="170px" Width="231px" Visible="false" ForeColor="Black">
                    <asp:Label ID="lblLogin" runat="server" Text="Área de Login"></asp:Label>
                    <br />
                    <asp:Label ID="lblUsuario" runat="server" Text="Usuário:"></asp:Label>
                    <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                    <br />
                    <asp:Label ID="lblSenha" runat="server" Text="Senha:"></asp:Label>
                    <asp:TextBox ID="txtSenha" runat="server"></asp:TextBox>
                    <br />
                    <asp:Button ID="btnVoltarDois" runat="server" OnClick="btnVoltarDois_Click" Text="Voltar" />
                    <asp:Button ID="btnEnviar" runat="server" OnClick="btnEnviar_Click" Text="Enviar" />
                    <asp:Label ID="lblMenssagemErro3" runat="server" ForeColor="Red"></asp:Label>
                </asp:Panel>
            </asp:Panel>

        </div>
    </form>
</body>
</html>
