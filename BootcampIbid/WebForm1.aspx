﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="BootcampIbid.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dias de trabalho</title>
</head>
<body>
    <form id="DiasParaTrabalhar" runat="server">
        <div style="width: 213px">
            <asp:Label ID="lblDiasdaSemana" runat="server" Text="Dias da semana" ForeColor="Black" Font-Bold="True" Font-Names="Book Antiqua" Font-Size="Larger" Font-Underline="False"></asp:Label>
        &nbsp;</div>
        <asp:CheckBoxList ID="chkDiasdaSemana" runat="server" BackColor="White" BorderColor="#000066" BorderStyle="Inset" ForeColor="#000066" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged" Width="133px" Font-Bold="True" Font-Names="Calibri" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False">
            <asp:ListItem>Domingo</asp:ListItem>
            <asp:ListItem>Segunda-Feira</asp:ListItem>
            <asp:ListItem>Terça-Feira</asp:ListItem>
            <asp:ListItem>Quarta-Feira</asp:ListItem>
            <asp:ListItem>Quinta-Feira</asp:ListItem>
            <asp:ListItem>Sexta-Feira</asp:ListItem>
            <asp:ListItem>Sábado</asp:ListItem>
        </asp:CheckBoxList>
        <p>
            <asp:Button ID="btnChkConfirmarDia" runat="server" Text="Confirmar" OnClick="btnConfirmarDia_Click" BackColor="#009933" Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Size="Medium" Font-Underline="False" ForeColor="Black" />
        </p>
        <p>
            <asp:Label ID="lblResultadoGravadoDias" runat="server" Font-Bold="True" Font-Italic="True" Font-Underline="False"></asp:Label>
        </p>
    </form>
</body>
</html>
