﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootcampIbid
{
    public partial class Panel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProximoUm_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtSobrenome.Text) || string.IsNullOrEmpty(txtGenero.Text) || string.IsNullOrEmpty(txtCelular.Text))
            {
                lblMenssagemErro.Text = "Todos os campos devem ser preenchidos.";
            }
            else
            {
                Panel2.Visible = false;
                Panel3.Visible = true;
                Panel4.Visible = false;
            }
            
        }

        protected void btnVoltarUm_Click(object sender, EventArgs e)
        {
                Panel2.Visible = true;
                Panel3.Visible = false;
                Panel4.Visible = false;
                lblMenssagemErro.Text = "";
        }

        protected void btnProximoDois_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtEndereco.Text) || string.IsNullOrEmpty(txtCidade.Text) || string.IsNullOrEmpty(txtCep.Text))
            {
                lblMenssagemErro2.Text = "Todos os campos devem ser preenchidos.";
            }
            else
            {
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = true;
            }

        }

        protected void btnVoltarDois_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = true;
            Panel4.Visible = false;
            lblMenssagemErro2.Text = "";
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtSenha.Text))
            {
                lblMenssagemErro3.Text = "Todos os campos devem ser preenchidos.";
            }
            else
            {
                txtUsuario.Text = "";
                txtSenha.Text = "";
                lblMenssagemErro3.Text = "";
            }
        }
    }
}