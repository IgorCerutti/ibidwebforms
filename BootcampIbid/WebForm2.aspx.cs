﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BootcampIbid
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableViewState = true;
        }

        protected void btnConfirmarOperacao_Click(object sender, EventArgs e)
        {
            string operacaoSelecionada = ddlOperacao.SelectedValue.ToString();


            if (operacaoSelecionada == "X")
            {
                if (double.TryParse(txtValorUm.Text, out double valorUm) && double.TryParse(txtValorDois.Text, out double valorDois))
                {
                    double resultado = valorUm * valorDois;

                    lblMenssagem.Text = $"Resultado da multiplicação {resultado}";
                    lblMenssagemErro.Text = "";
                }
                else
                {
                    lblMenssagemErro.Text = "Voce deve digitar um número, caracteres não são válidos!";
                    lblMenssagem.Text = "";

                }
            }
            else if (operacaoSelecionada == "-")
            {
                if (double.TryParse(txtValorUm.Text, out double valorUm) && double.TryParse(txtValorDois.Text, out double valorDois))
                {
                    double resultado = valorUm - valorDois;

                    lblMenssagem.Text = $"Resultado da multiplicação {resultado}";
                    lblMenssagemErro.Text = "";
                }
                else
                {
                    lblMenssagemErro.Text = "Voce deve digitar um número, caracteres não são válidos!";
                    lblMenssagem.Text = "";
                }
            }
            else if (operacaoSelecionada == "+")
            {
                if (double.TryParse(txtValorUm.Text, out double valorUm) && double.TryParse(txtValorDois.Text, out double valorDois))
                {
                    double resultado = valorUm + valorDois;

                    lblMenssagem.Text = $"Resultado da multiplicação {resultado}";
                    lblMenssagemErro.Text = "";
                }
                else
                {
                    lblMenssagemErro.Text = "Voce deve digitar um número, caracteres não são válidos!";
                    lblMenssagem.Text = "";
                }
            }
            else if (operacaoSelecionada == "/")
            {
                if (double.TryParse(txtValorUm.Text, out double valorUm) && double.TryParse(txtValorDois.Text, out double valorDois))
                {
                    double resultado = valorUm / valorDois;

                    lblMenssagem.Text = $"Resultado da multiplicação {resultado}";
                    lblMenssagemErro.Text = "";
                }
                else
                {
                    lblMenssagemErro.Text = "Voce deve digitar um número, caracteres não são válidos!";
                    lblMenssagem.Text = "";

                }
            }
        }
    }
}